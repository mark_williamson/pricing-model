import {mount, shallow} from "enzyme/build";
import PricingTiers from "./PricingTiers";
import React from "react";

it('should render correctly as a pricing tier', () => {
    const onChange = jest.fn();
    const component = shallow(<PricingTiers value={'standard'} onChange={onChange} />);

    expect(component).toMatchSnapshot();
});

it('when the tier is clicked onChange is called', () => {

    const testState = { tier: 'standard' };

    const component = shallow(<PricingTiers
        value={'standard'}
        onChange={(e) => {
            testState['tier'] = e;
        }}

    />);

        console.log(component.debug());
        const radioButton = component
            .find('#radioGroup');


    radioButton.simulate("change", { target: { value: "enterprise" }});

    expect(testState.tier).toEqual('enterprise');

});
