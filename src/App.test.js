import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders basic app', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/Standard Edition/i);
  expect(linkElement).toBeInTheDocument();
});
