import {mount, shallow} from "enzyme/build";
import TotalPrice from "./TotalPrice";
import React from "react";

it('should render correctly as a pricing tier', () => {
    const component = shallow(<TotalPrice price={13} />);

    expect(component).toMatchSnapshot();
});

it('The total should display the price', () => {
    const component = shallow(<TotalPrice price={13} />);


    expect(
        component.containsMatchingElement(<span>{13}</span>)
    ).toBeTruthy();

});
