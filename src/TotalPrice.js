import React, { Fragment} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';





const useStyles = makeStyles({
    priceBox: {
        borderRadius: 4,
        backgroundColor: '#f7f5f9',
        maxWidth: 140,
        padding: 13,
    },
    label: {
        color: '#a7a5aa'
    }
});


function TotalPrice(props) {

    const classes = useStyles();
    const { price} = props;

    return (
        <Fragment >
            <div className={classes.label}>Total Price</div>
            <div className={classes.priceBox}>$<span>{price}</span> per month    </div>
        </Fragment>
    )
}


TotalPrice.propTypes = {
    price: PropTypes.number.isRequired,
};

export default (TotalPrice);
