import React, { Fragment} from 'react';
import PropTypes from 'prop-types';

import {
    Radio,
    RadioGroup,
    FormControlLabel,
    FormControl,
    }
    from '@material-ui/core';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';




const useStyles = makeStyles({
    root: {
        '&:hover': {
            backgroundColor: 'transparent',
        },
    },
    tiers: {
        marginBottom: 20,
    },
    icon: {
        borderRadius: '50%',
        width: 16,
        height: 16,
        backgroundColor: '#f5f8fa',
        '$root.Mui-focusVisible &': {
            outline: '2px auto rgba(19,124,189,.6)',
            outlineOffset: 2,
        },
        'input:hover ~ &': {
            backgroundColor: '#ebf1f5',
        },
        'input:disabled ~ &': {
            boxShadow: 'none',
            background: 'rgba(206,217,224,.5)',
        },
    },
    checkedIcon: {
        backgroundColor: '#2010fc',
        '&:before': {
            display: 'block',
            width: 16,
            height: 16,
            backgroundImage: 'radial-gradient(#fff,#fff 28%,transparent 32%)',
            content: '""',

        },
        'input:hover ~ &': {
            backgroundColor: '#fc1423',
        },

    },
});




function PricingTiers(props) {

    const handleChange = (event) => {

        const {onChange} = props;
        onChange(event.target.value)
    };

    const classes = useStyles();
    const  {value} = props;

    return (
        <Fragment>

            <FormControl
                component="fieldset"
                className={classes.tiers}
            >
                <RadioGroup
                    aria-label="pricingtier"
                    name="pricingtier"
                    value={value}
                    id={'radioGroup'}
                    onChange={handleChange}>
                        <FormControlLabel
                            value="standard"
                            id='standard'
                            control={<Radio
                                    className={classes.root}
                                    disableRipple
                                    id='standardRadio'
                                    color="default"
                                    icon={<span className={classes.icon} />}
                                    checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
                            />}
                            label="Standard Edition"
                        />
                        <FormControlLabel
                            value="enterprise"
                            id='enterprise'
                            control={<Radio
                                className={classes.root}

                                disableRipple
                                id='enterpriseRadio'
                                color="default"
                                icon={<span className={classes.icon} />}
                                checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
                            />}
                            label="Enterprise Edition"
                        />
                </RadioGroup>
            </FormControl>
        </Fragment>
    )
};


PricingTiers.propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
};

export default (PricingTiers);
