import React, {Fragment} from 'react';
import PricingTiers from "./PricingTiers";
import TotalPrice from "./TotalPrice";




const pricingTiers = [
    { key: 'standard', price: 120 },
    { key: 'enterprise', price: 999 },
];

function PricingCoordinator(props) {

    const [value, setValue] = React.useState('standard');

    const getPrice = (tier) => {
        return pricingTiers.find(t => t.key === tier).price;
    };

    return (
        <Fragment>
            <PricingTiers
                onChange={setValue}
                value={value}
            />
            <TotalPrice
                price={getPrice(value)}
            />
        </Fragment>
    )
}





export default (PricingCoordinator);
